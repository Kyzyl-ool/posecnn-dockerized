FROM pytorch/pytorch:1.9.0-cuda10.2-cudnn7-devel

RUN apt -y update && apt-get -y update && apt -y install cmake

RUN apt install wget

RUN apt install unzip

# git
RUN apt -y install git

# eigen
RUN cd /workspace && git clone https://gitlab.com/libeigen/eigen.git && \
    cd eigen && \
    mkdir build && \
    cd build && \
    cmake .. && \
    make && \
    make install

# Sophus
RUN cd /workspace && git clone https://github.com/strasdat/Sophus && \
    cd Sophus && \
    mkdir build && \
    cd build && \
    cmake .. && \ 
    make && \
    make install

# PoseCNN-PyTorch
RUN cd /workspace && git clone https://github.com/NVlabs/PoseCNN-PyTorch && cd PoseCNN-PyTorch && pip install -r requirement.txt 

RUN cd /workspace/PoseCNN-PyTorch/ycb_render && git submodule update --init --recursive

RUN cd /workspace/PoseCNN-PyTorch/lib/layers && python3 setup.py install

RUN cd /workspace/PoseCNN-PyTorch/lib/utils && python3 setup.py build_ext --inplace

RUN cd /workspace/PoseCNN-PyTorch/ycb_render && sudo python3 setup.py develop


