This repository is not an original official implementation of the work, but a refactored codebase.Performed within the FSE coursework at Skoltech.
Link to the original repo: https://github.com/NVlabs/PoseCNN-PyTorch

**Quickstart**

```bash
git clone git@gitlab.com:Kyzyl-ool/posecnn-dockerized.git
cd posecnn-dockerized
git submodule init
git submodule update
docker pull bavanesyan/posecnn_fse_project:latest
docker run -it <IMAGE ID>
```

**Training your own models**
```bash
cd $ROOT
```
1. Download images, and save to $ROOT/data or use symbol links:
```bash
./scripts/download.sh '1Q5VTKHEEejT2lAKwefG00eWcrnNnpieC' 'data/backgrounds.zip' 

Where '1Q5VTKHEEejT2lAKwefG00eWcrnNnpieC' is id of file from google drive and 'data/backgrounds.zip' is destination

Prepare data:

./scripts/prepare.sh data/backgrounds.zip
```

2. Download pretrained VGG16 weights. Put the weight file to $ROOT/data/checkpoints.
```bash
./scripts/download.sh
```
3. Training and testing for 20 YCB objects with synthetic data. Modify the configuration file for training on a subset of these objects.

\# multi-gpu training, use 1 GPU or 2 GPUs since batch size is set to 2
```bash
./scripts/train.sh
```
\# testing, $GPU_ID can be 0, 1, etc.
```bash
./scripts/eval.sh $GPU_ID
```
**Development**

This repository uses PoseCNN repository (https://github.com/NVlabs/PoseCNN-PyTorch/) as submodule. After you initialized the submodule, you can develop in the `PoseCNN-PyTorch` folder as usual. You can checkout into some branch in submodule:
```bash
cd PoseCNN-PyTorch
git checkout -b my-new-branch
```

and build Docker image

```bash
docker build .
```

After image is built you can push it into DockerHub:

```bash
docker push bavanesyan/posecnn_fse_project:your_tag
```
