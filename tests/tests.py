import os
import unittest

from utils.load import download_file_from_google_drive

import pathlib as pl


class TestScripts(unittest.TestCase):
    def test_download(self):
        file_id = '1tTd64s1zNnjONlXvTFDZAf4E68Pupc_S'
        destination = 'data/vgg16-397923af.pth'
        download_file_from_google_drive(file_id, destination)
        if not pl.Path(destination).resolve().is_file():
            raise AssertionError("File does not download: %s" % str(destination))
        os.remove(destination)


if __name__ == '__main__':
    unittest.main()
