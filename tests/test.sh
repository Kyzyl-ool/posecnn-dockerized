#!/bin/sh

python3 tests.py

AMOUNT=$(ls -l ../PoseCNN-PyTorch | wc -l | sed -E 's/[^0-9]*([0-9]+).*/\1/')

if [ $AMOUNT == 0 ]
then
    echo "Failed"
    exit 1
else
    echo "Success"
    exit 0
fi
