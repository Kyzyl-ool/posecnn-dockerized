#!/bin/bash

set -x
set -e

./PoseCNN-PyTorch/tools/train_net.py \
  --network posecnn \
  --pretrained ./PoseCNN-PyTorch/data/checkpoints/vgg16-397923af.pth \
  --dataset ycb_object_train \
  --cfg ./PoseCNN-PyTorch/experiments/cfgs/ycb_object.yml \
  --solver sgd \
  --epochs 16
