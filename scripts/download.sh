#!/usr/bin/env python3

import sys

from utils.load import download_file_from_google_drive

if __name__ == "__main__":
    file_id = sys.argv[1]
    destination = sys.argv[2]
    download_file_from_google_drive(file_id, destination)