#!/bin/bash

set -x
set -e

export PYTHONUNBUFFERED="True"
export CUDA_VISIBLE_DEVICES=$1

./PoseCNN-PyTorch/tools/test_net.py --gpu $1 \
  --network posecnn \
  --pretrained ./output/ycb_object/ycb_object_train/vgg16_ycb_object_epoch_$2.checkpoint.pth \
  --dataset ycb_object_test \
  --cfg ./PoseCNN-PyTorch/experiments/cfgs/ycb_object.yml
